# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls --color=auto -l'
alias cp='cp -i'
alias df='df -h'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias free='free -m'
alias grep='grep --colour=auto'
alias more='less'
alias np='nano -w PKGBUILD'
alias sx='startx'
alias tmux='tmux -u'
alias ytview='youtube-viewer --colorful --quiet'
alias neof='neofetch --ascii ~/.config/neofetch/Tux-dab'
alias vim='nvim'

#PS1='┌ \u@\h - [ \W ] | ----------\n└─> enter command - $  '
PS1='┌──[ \[\e[1;97m\]\u\[\e[m\] ]───[ \[\e[1;97m\]\h\[\e[m\] ] \[\e[1;94m\][\w]\[\e[m\]
└───\[\e[1;97m\]>>\[\e[m\] '

# >>> Added by cnchi installer
BROWSER=/usr/bin/qutebrowser
EDITOR=/usr/bin/nvim

