# ZSHRC
autoload -Uz compinit promptinit
emulate sh -c 'source /etc/profile'
bindkey -v
compinit
promptinit

# Prompt
export PS1="┌──[ %B%U%n%b%u ]───[ %B%U%m%b%u ]%F{blue}───[%B%U%~%b%u]%f
└───>> "

# Defaults
export HISTFILE=~/.zsh_history
export HISTSIZE=50
export SAVEHIST=50
export TERMINAL="urxvt"
export READER="zathura"
export BROWSER="/usr/local/bin/qutebrowser"
export EDITOR="vimx"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto -lh'
alias cp='cp -i'
alias df='df -h'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias free='free -m'
alias grep='grep --colour=auto'
alias more='less'
alias np='nano -w PKGBUILD'
alias sx='startx'
alias tmux='tmux -u'
alias ytview='youtube-viewer --colorful --quiet'
alias neof='neofetch --ascii ~/.config/neofetch/Tux-dab'
alias nv='nvim'
alias weather='curl "wttr.in/reno?m"'
alias moon='curl "wttr.in/moon"'
alias upgrade='sudo dnf upgrade'
alias journalctl='journalctl -r'
alias :e='vimx'
alias :q='exit'

# Faster! (?)
zstyle ':completion::complete:*' use-cache 1

# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# generate descriptions with magic.
zstyle ':completion:*' auto-description 'specify: %d'

# color code completion!!!!  Wohoo!
zstyle ':completion:*' list-colors "=(#b) #([0-9]#)*=36=31"

# Egomaniac!
zstyle ':completion:*' list-separator 'fREW'

# autocomplete
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

# autocd
setopt autocd
