#!/usr/bin/env sh

main() {
    case $1 in
        full) full ;;
        sel) sel ;;
        *) echo "Unknown command '$@'." ;;
    esac
}

full() {
    maim --hidecursor /tmp/screenshot.png ;
    imgur.sh /tmp/screenshot.png | xclip -selection clipboard ;
    notify-send "Uploaded full screenshot to Imgur"
}

sel() {
    maim --select --hidecursor /tmp/screenshot.png ;
    imgur.sh /tmp/screenshot.png | xclip -selection clipboard ;
    notify-send "Uploaded selected screenshot to Imgur"
}

main "$@"
