if [ $(amixer sget Capture | grep 'Right:\|Mono:' | grep -o off) = "off" ]; then
	notify-send "Mic unmuted" && amixer set Capture toggle
else
	notify-send "Mic muted" && amixer set Capture toggle
fi
