#!/usr/bin/env sh


print_mem(){
    memfree=$(($(grep -m1 'MemAvailable:' /proc/meminfo | awk '{print $2}') / 1024))
    echo -e "$memfree"
}

print_temp(){
    test -f /sys/class/thermal/thermal_zone0/temp || return 0
    echo $(head -c 2 /sys/class/thermal/thermal_zone0/temp)C
}

print_bat(){
    hash acpi || return 0
    onl="$(grep "on-line" <(acpi -V))"
    charge="$(awk '{ sum += $1 } END { print sum }' /sys/class/power_supply/BAT*/capacity)"
    if test -z "$onl"
    then
        # suspend when we close the lid
        #systemctl --user stop inhibit-lid-sleep-on-battery.service
        echo -e "${charge}"
    else
        # On mains! no need to suspend
        #systemctl --user start inhibit-lid-sleep-on-battery.service
        echo -e "${charge}"
    fi
}

print_date(){
	date "+%a %m-%d %T%:::z"
}

print_volume() {
	volume="$(amixer get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/')"
	if test "$volume" -gt 0
	then
		echo -e "${volume}"
	else
		echo -e "Mute"
	fi
}

while true
do
    echo "| Memory = $(print_mem) | Hotness = $(print_temp) | Volume = $(print_volume) | Battery = $(print_bat) | Date = $(print_date) |"

    old_time=$now

    sleep 1

done
