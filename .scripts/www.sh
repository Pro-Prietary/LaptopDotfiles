#!/usr/bin/env sh

# Definitions
term="urxvt"
shell="/usr/bin/env sh"

choices="DuckDuckGo
ProtonDB
Bank
Amazon
RenoJazz
GitLab
GuerillaMail
AlternativeTo
4chan
Aberoth
GOG
Go
Lutris"

chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-World Wide Web-###")

case "$chosen" in
    DuckDuckGo) surf https://duckduckgo.com/ ;;
    ProtonDB) surf https://www.protondb.com/;;
    Aberoth) surf https://aberoth.com/ ;;
    Bank) surf https://www.bankofamerica.com/ ;;
    RenoJazz) surf http://www.renojazz.org/ ;;
    GOG) surf https://www.gog.com/ ;;
    GuerillaMail) surf https://www.guerrillamail.com/ ;;
    Go) surf https://online-go.com/ ;;
    Lutris) surf https://lutris.net/ ;;
    AlternativeTo) surf https://alternativeto.net/ ;;
    Nextcloud) surf http://24.241.135.51/nextcloud/index.php/login/ ;;
    Amazon) surf https://www.amazon.com/ ;;
    GitLab) surf https://about.gitlab.com/ ;;

    Reddit) surf https://www.reddit.com/ ;;
    4chan) surf https://www.4chan.org/ ;;


esac
