term="urxvt"
directory=~/Git_Repo/dline
pyver=3.6
launcher=launcher.py
shell=/bin/bash

if [[ $(tmux list-sessions | grep -o discord) ]]; then
    $term -title Dline -e sh -c "tmux -2 attach-session -d -t discord"
else 
    tmux new-session -d -s discord "cd $directory && exec python$pyver dline.py" &&
    $term -title Dline -e sh -c "tmux -2 attach-session -d -t discord"
fi
