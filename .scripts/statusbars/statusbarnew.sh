#!/usr/bin/env sh

# Colour names
WHITE=ffffff
LIME=00ff00
GRAY=666666
YELLOW=ffff00
MAROON=cc3300

print_win(){
    window=( $(xprop -id $(xprop -root _NET_ACTIVE_WINDOW | cut -d\  -f5) _NET_WM_NAME WM_CLASS | sed 's/.*\ =\ "\|\",\ \".*\|"$//g;s/\\\"/"/g') )
    echo -e "$window"
}

############        BROKEN AF
#print_mpd(){
    #mpd=( $(mpc -f ' [%title%|%file%]\n %artist%\n %album%') )
    #state=$([[ ${mpd[3]/ *} == '[playing]' ]] && echo 'PLAY' || echo 'PAUS')
    #volume=$(echo ${mpd[4]} | cut -d\  -f2)
    #if [[ ${MPD[0]} != ' ' ]]; then
        #echo $STATE  $LIME
        #echo ${MPD[0]}
    #if [[ ${MPD[1]} != ' ' ]]; then
        #echo ' by'   $GRAY
        #echo ${MPD[1]}
    #fi
    #if [[ ${MPD[2]} != ' ' ]]; then
        #echo ' on'   $GRAY
        #echo ${MPD[2]}
    #fi
#}

print_mem(){
    memory=$(free -h | awk '/^Mem:/ {print $3 "/" $2}')
    echo -e "$memory"
}

print_temp(){
    cputemp=$(sensors | awk '/^CPU:/ {print $2}')
    echo -e "$cputemp"
}

print_volume(){
    volume="$(amixer get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/')"
    if test "$volume" -gt 0
    then
        echo -e "${volume}"
    else
        echo -e "Mute"
    fi
}

print_bat(){
    capacity=$(cat /sys/class/power_supply/BAT0/capacity)
    ac=$(cat /sys/class/power_supply/AC/online)
    if [ "$ac" = 0 ]; then
        echo -e "$capacity"
    else
        echo -e "$capacity+AC"
    fi
}

print_date(){
    date "+%Y-%m-%d %R:%S"
}

while true
do
    echo "$(print_win) | $(print_mpd) | Memory = $(print_mem) | CPUTemp = $(print_temp) | Volume = $(print_volume) | Battery = $(print_bat) | Date = $(print_date)"

    sleep 1
done
