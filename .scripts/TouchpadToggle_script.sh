#!/usr/bin/env sh

# Definitions
TERM="urxvt"

if [ $(xinput list-props "Synaptics TM3096-001" | grep "Device Enabled" | grep -o "0") = "0" ]; then
    xinput enable "Synaptics TM3096-001" &&
    xinput set-prop "Synaptics TM3096-001" "libinput Tapping Enabled" 1 &&
    notify-send "Touchpad ON"
else
    xinput disable "Synaptics TM3096-001" &&
    xinput set-prop "Synaptics TM3096-001" "libinput Tapping Enabled" 0 &&
    notify-send "Touchpad OFF" &&
    xdotool mousemove --polar 60 790
fi
