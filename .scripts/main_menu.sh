#!/usr/bin/env sh

# Definitions
term="urxvt"
shell="/usr/bin/env sh"

main() {
    case $1 in
        audio) audio ;;
        bighogs) bighogs ;;
        chat) chat ;;
        game) game ;;
        internet) internet ;;
        power) power ;;
        theme) theme ;;
        unmount) umount ;;
        *) echo "Unknown command '$@'." ;;
    esac
}

audio() {
    choices="Cava\nPlayer\nAdd_Music\nClear_Music\nShuffle_Music\nPulsemixer\nAlsamixer"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Audio Menu-###")

    case "$chosen" in
        Player) notify-send "Running NCMPCPP" && $term -e sh -c ncmpcpp ;;
        Cava) notify-send "Running Cava" && exec ~/.scripts/music_script.sh cava ;;
        Pulsemixer) notify-send "Running Pulsemixer" && $term -e sh -c pulsemixer ;;
        Alsamixer) notify-send "Running Alsamixer" && $term -title Alsamixer -e sh -c alsamixer ;;
        Add_Music) notify-send "Adding music" && mpc add / ;;
        Clear_Music) notify-send "Clearing music" && mpc clear ;;
        Shuffle_Music) notify-send "Shuffling Music" && mpc shuffle ;;
    esac
}

bighogs() {
    notify-send "Biggest Memory Hogs
---------------------
$(ps axch -o cmd:15,%mem --sort=-%mem | head)"
}

chat(){
    choices="Discord\nWeechat\nTerminal-Discord"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Messenger Menu-###")

    case "$chosen" in
        Discord) notify-send "Running Discord" && exec discord ;;
        Weechat) exec ~/.scripts/weechat_script.sh ;;
        Terminal-Discord) notify-send "Running Terminal-Discord" && exec "~/.scripts/dline_script.sh" ;;
    esac
}

game() {
    choices="Lutris\nGameBoy\nSteam"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Game Library-###")

    case "$chosen" in 
        Lutris) notify-send "Running Lutris. Have Fun!" && exec lutris ;;
        GameBoy) notify-send "Running mGBA" && exec mgba-qt ;;
        Steam) notify-send "Running Steam. Have Fun!" && steam ;;
    esac
}

internet() {
    choices="Chrome\nFirefox\nQutebrowser\nElinks"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Internet Menu-###")

    case "$chosen" in
        Qutebrowser) notify-send "Running Qutebrowser" && qutebrowser ;;
        Firefox) notify-send "Running Firefox" && firefox ;;
        Elinks) notify-send "Running Elinks" && ~/.scripts/elinks_script.sh ;;
        Chrome) notify-send "Running Chrome" && google-chrome-stable ;;
    esac
}

power() {
    choices="Shutdown\nReboot\nExit"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Power Menu-###")

    case "$chosen" in 
        Shutdown) poweroff ;;
        Reboot) reboot ;;
        Exit) pkill x ;;
    esac
}

theme() {
    choices="Random\nKrustyKrabPizza\nBlueShotgunKin\nMountBlade\nPeytoLake\nQuattuor\nRubiksCube\nStarfighters\nStormtrooper\nCrash\nStarDestroyer\nFire"

    chosen=$(echo -e "$choices" | rofi -dmenu -i -p "###-Theme Menu-###")

    case "$chosen" in
        Random) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures ;;
        KrustyKrabPizza) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/KrustyKrabPizza.jpg ;;
        BlueShotgunKin) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/BlueShotgunKin.jpg ;;
        MountBlade) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/MountBlade.jpg ;;
        PeytoLake) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/PeytoLake.jpg ;;
        Quattuor) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/Quattuor.jpg ;;
        RubiksCube) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/RubiksCube.jpg ;;
        Starfighters) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/Starfighters.jpg ;;
        Stormtrooper) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/Stormtrooper.jpg ;;
        Crash) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/Crash.jpg ;;
        StarDestroyer) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/StarDestroyer.jpg ;;
        Fire) notify-send "Changing Theme" && exec wal -a 90 -i ~/Pictures/Background_Pictures/Fire.jpg ;;
    esac
}

umount() {
    udiskie-umount /run/media/cyrano/*
}

main "$@"
