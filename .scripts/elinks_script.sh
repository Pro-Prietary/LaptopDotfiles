#!/usr/bin/env sh
term="urxvt"
shell=/bin/bash

if [[ $(tmux list-sessions | grep -o elinks) ]]; then
    $term -e sh -c "tmux -2 attach-session -d -t elinks"
else 
    tmux new-session -d -s elinks 'elinks duckduckgo.com' &&
    sleep 2 &&
    $term -e sh -c "tmux -2 attach-session -d -t elinks"
fi
