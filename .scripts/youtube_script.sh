term='urxvt'
shell='/bin/bash'

if [ $(tmux list-sessions | grep -o youtube-client) ]; then
    $term -title Youtube -e sh -c "tmux -2 attach-session -d -t youtube-client"
else
    notify-send "Running youtube-viewer" &&
    tmux new-session -d -s youtube-client -n youtube-client 'mpsyt' &&
    $term -title Youtube -e sh -c "tmux -2 attach-session -d -t youtube-client"
fi
