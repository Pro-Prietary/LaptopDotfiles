term='urxvt'
shell='/bin/bash'

if [ $(tmux ls | grep -o reddit) ]; then
    $term -title Reddit -e sh -c "tmux -2 attach-session -d -t reddit"
else
    notify-send "Running Reddit-Terminal-Viewer" &&
    tmux new-session -d -s reddit -n rtv 'rtv --no-flash' &&
    $term -title Reddit -e sh -c "tmux -2 attach-session -d -t reddit"
fi
