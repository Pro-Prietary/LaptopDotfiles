term='urxvt'
shell='/bin/bash'

if [ $(tmux ls | grep -o weechat) ]; then
    sleep 0.5 &&
    $term -title Weechat -e sh -c "tmux -2 attach-session -d -t weechat"
else
    sleep 0.5 &&
    notify-send "Running Weechat" &&
    tmux new-session -d -s weechat -n weechat 'weechat' &&
    sleep 2 &&
    $term -title Weechat -e sh -c "tmux -2 attach-session -d -t weechat"
fi
